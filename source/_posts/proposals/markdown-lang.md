---
title: Markdown-based язык для описания контента работ
date: 2021/2/10 21:09:00
categories:
    - Proposals
tags: 
    - process-model
---

# Предложение

Реализовать собственный язык на основе Markdown синтаксиса и транслятор его в latex. 

*Требования:*
1. Более удобный для чтения синтаксис, чем латех
1. Специальный синтаксис оформления таблиц
1. Специальный синтаксис оформления рисунков
1. Возможность использовать сторонние программы для генерации рисунков и таблиц: plantUML, mermaid, graphviz, CSV.
1. Возможность задавать ссылки в латех-нотации (label, ref, cite).

<!-- more -->

# Мотивация

- Использовать $`\LaTeX`$ в чистом виде достаточно сложно.
- Пользователям Word проще будет использовать markdown, чем латех, вследствие более простого и чёткого синтаксиса.
- $`\LaTeX`$ предоставляет излишнее количество функционала, некорректное использование которого может привести к ошибкам в оформлении (`itemize` вместо `compactitem`, сложная вложенность окружений при вставке картинки).

# Концепт

* В качестве базового синтаксиса используется [Gitlab-markdown](https://docs.gitlab.com/ee/user/markdown.html).

* Запрещается использовать GFM-таблицы, так как они не позволяют писать в ячейке несколько строк.

* Вставка изображений следующим кодом:  
    ```md
    ![About](url){50%}
    ```
    Параметр в фигурных скобках будет задавать ширину картинки относительно ширины строки в документе.

* Ссылки (`label`, `ref`, `cite` функционал):
    ```md
    ## Header @label(label-name)
    In section @ref(label-name) we use citation @cite(cite-name).
    ```

* Умная директива `@input`
    * Вставка картинок: `@input(image.jpg, "About", size=50%, label=imagename)`
    * Вставка таблиц: `@input(data.csv, "Stats")`
    * Вставка листингов: `@input(main.cpp)`

Синтаксис таблиц:

```md
@cell(cell-name-1)
Test of text and text

Another $`some`$
@end-cell

@line(line-name-1)
* first
* second
* third
@end-line

@column(column-some-1)
* one
* two
@end-column

@cells(cells-1, data.csv)

@table(my-table, "My data")
@:cell-name-1   @:line-name-1
@:column-some-1 @:cells-1
@end-table
```